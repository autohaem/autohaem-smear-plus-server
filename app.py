from labthings import ActionView, PropertyView, create_app, fields, find_component, op
from labthings.json import encode_json
from autohaem_smear_plus.autohaem_smear_plus import AutohaemSmearPlus


class IdProperty(PropertyView):
    schema = fields.String(required=True)
    semtype = "LevelProperty"

    @op.readproperty
    def get(self):
        my_component = find_component("org.autohaem.smearplus")
        return my_component.id


class SmearAction(ActionView):
    def post(self):
        my_component = find_component("org.autohaem.smearplus")
        return my_component.smear()

class MoveAction(ActionView):
    args = {
        "throttle": fields.Float(missing = 1, example = 1, minimum = 0, maximum = 1, description = "The throttle of the motor"),
        "duration": fields.Float(missing = 1, example = 1, minimum = 0, maximum = 10, description = "The time to move the slider (s)")
    }
    def post(self, args):
        my_component = find_component("org.autohaem.smearplus")

        throttle = args.get("throttle")
        time = args.get("duration")
        return my_component.move(throttle, time)


app, labthing = create_app(
    __name__,
    title="Autohaem Smear Plus API",
    description="Autohaem Smear Plus automated smearer API",
    version = "0.0.0"
)

autohaem_smear_plus = AutohaemSmearPlus()
labthing.add_component(autohaem_smear_plus, "org.autohaem.smearplus")


labthing.add_view(IdProperty, "/properties/id")
labthing.add_view(SmearAction,"/actions/smear")
labthing.add_view(MoveAction,"/actions/move")

# Start the app
if __name__ == "__main__":
    from labthings import Server

    Server(app).run()