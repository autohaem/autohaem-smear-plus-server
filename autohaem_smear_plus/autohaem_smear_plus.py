import logging
import time
from adafruit_motorkit import MotorKit


class AutohaemSmearPlus:

    def __init__(self):
        self.id: str = f"autohaem_smear_plus"
        self.name:  str = self.id 
        self.smear_length: int= 30
        self.motor_rpm: int = 500
        self.motor_pitch: int = 0.5
        try:
            self.motor = MotorKit()
        except: 
            logging.error("No motor driver connected")
        

    def smear(self):
        self.motor.motor1.throttle = 0.5
        time.sleep(1)
        self.motor.motor1.throttle = -1
        time.sleep(8)
        self.motor.motor1.throttle = 0

    def move(self,throttle,duration):
        self.motor.motor1.throttle = throttle
        time.sleep(duration)
        self.motor.motor1.throttle = 0

